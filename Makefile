install:
	docker exec -it infogest-frontend bash -c "yarn install"

build:
	docker exec -it infogest-frontend bash -c "yarn build"