const API_URL = 'https://dreamfactory.nvncompany.ro/'

const getApiUrl = endpoint => `${API_URL}${endpoint}`

export default {
  methods: {
    getApiUrl(endpoint, params) {
      return getApiUrl(endpoint, params)
    },
  },
}
