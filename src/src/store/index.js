import Vue from 'vue'
import Vuex from 'vuex'

import user from './user'
import layout from './layout'

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  return new Vuex.Store({
    modules: {
      user,
      layout,
    },
  })
}
