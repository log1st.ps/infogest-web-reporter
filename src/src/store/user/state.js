import { LocalStorage } from 'quasar'

const login = LocalStorage.get.item('login')
const token = LocalStorage.get.item('token')

export default {
  isAuthorized: !!login,
  isSignInModalActive: false,
  token,
  login,
}
