import { LocalStorage } from 'quasar'

export const isSignInModalActive = (state, value) => {
  state.isSignInModalActive = value
}

export const isAuthorized = (state, { token, login }) => {
  state.isAuthorized = true
  state.token = token
  state.login = login

  LocalStorage.set('token', token)
  LocalStorage.set('login', login)
}

export const signOut = (state) => {
  state.isAuthorized = false
  state.token = null
  state.login = null

  LocalStorage.remove('token')
  LocalStorage.remove('login')
}
