export default {
  header: {
    title: 'InfoGest Web Reporter',
    subTitle: 'Signed in as {user}',
    menuAria: 'Toggle menu on left side',
    actions: {
      signIn: 'Sign In',
      signOut: 'Sign Out',
    },
  },
  sidebar: {
    menu: {
      main: 'Main',
      items: {
        main: {
          dashboard: 'Dashboard',
        },
      },
    },
  },
  signIn: {
    title: 'Authorization',
    field: {
      login: 'Login',
      password: 'Password',
    },
    submit: 'Submit',
  },
}
