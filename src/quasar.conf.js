module.exports = ctx => ({
  plugins: [
    'i18n',
    'axios',
  ],
  css: [
    'app.styl',
  ],
  extras: [
    ctx.theme.mat ? 'roboto-font' : null,
    'material-icons',
  ],
  supportIE: false,
  build: {
    scopeHoisting: true,
    vueRouterMode: 'history',
    vueCompiler: true,
    gzip: true,
    analyze: false,
    extractCSS: false,
    extendWebpack(cfg) {
      cfg.module.rules.push({
        enforce: 'pre',
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        exclude: /node_modules/,
      })
    },
  },
  devServer: {
    open: true,
  },
  framework: {
    components: [
      'QLayout',
      'QLayoutHeader',
      'QLayoutDrawer',
      'QPageContainer',
      'QPage',
      'QToolbar',
      'QToolbarTitle',
      'QBtn',
      'QIcon',
      'QList',
      'QListHeader',
      'QItem',
      'QItemMain',
      'QItemSide',
      'QScrollArea',
      'QModal',
      'QField',
      'QInput',
      'QTable',
      'QTh',
      'QTr',
      'QTd',
      'QTableColumns',
    ],
    directives: [
      'Ripple',
    ],
    plugins: [
      'Notify',
      'LocalStorage',
    ],
    iconSet: ctx.theme.mat ? 'material-icons' : 'ionicons',
  },
  animations: 'all',
  ssr: {
    pwa: true,
  },
  pwa: {
    // workboxPluginMode: 'InjectManifest',
    // workboxOptions: {},
    manifest: {
      name: 'InfoGest Web Report',
      short_name: 'InfoGest-WReport',
      description: 'InfoGest Web Report Based on InfoGest API Documentation',
      display: 'standalone',
      orientation: 'portrait',
      background_color: '#ffffff',
      theme_color: '#027be3',
      icons: [
        {
          src: 'statics/icons/icon-128x128.png',
          sizes: '128x128',
          type: 'image/png',
        },
        {
          src: 'statics/icons/icon-192x192.png',
          sizes: '192x192',
          type: 'image/png',
        },
        {
          src: 'statics/icons/icon-256x256.png',
          sizes: '256x256',
          type: 'image/png',
        },
        {
          src: 'statics/icons/icon-384x384.png',
          sizes: '384x384',
          type: 'image/png',
        },
        {
          src: 'statics/icons/icon-512x512.png',
          sizes: '512x512',
          type: 'image/png',
        },
      ],
    },
  },
})
